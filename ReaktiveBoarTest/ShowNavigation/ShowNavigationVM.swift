//
//  ShowNavigationVM.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 6/12/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit

class ShowNavigatiionVM: VCVM {

    let next = PassthroughSubject<Void, Never>()
    let back = PassthroughSubject<Void, Never>()
    let nnn: Int
    static var num = 0
    required init(resolver: Resolver) {
        ShowNavigatiionVM.num += 1
        nnn = ShowNavigatiionVM.num

        super.init(resolver: resolver)
        let router = resolver~>RouterProtocol1.self

        next.bind(to: router.show(ShowNavigatiionVM.self))
            .dispose(in: reactive.bag)

        back.bind(to: router.close())
            .dispose(in: bag)
    }
}
