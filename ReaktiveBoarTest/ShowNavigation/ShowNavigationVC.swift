//
//  ShowNavigationVC.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 6/12/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

extension UINavigationController: NavigationConfigProtocol {
    public func configure() {
        self.isNavigationBarHidden = true
    }
}

class ShowNavigationVC: UIViewController {
    @IBOutlet weak var back1: UIButton!
    @IBOutlet weak var next1: UIButton!
}

extension ShowNavigationVC: IgnorePrevous1 { }

extension ShowNavigationVC: VCView {
    func advise(vm: ShowNavigatiionVM, bag: DisposeBag) {

        back1.reactive.tap
            .bind(to: vm.back)
            .dispose(in: bag)

        next1.reactive.tap
            .bind(to: vm.next)
            .dispose(in: bag)
    }

    typealias VMType = ShowNavigatiionVM
}
