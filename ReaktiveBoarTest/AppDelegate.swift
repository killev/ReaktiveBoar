//
//  AppDelegate.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 5/25/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import ReaktiveBoar

extension UIApplication {
    static var isTesting: Bool {
        #if DEBUG
        return ProcessInfo.processInfo
            .environment["XCTestConfigurationFilePath"] != nil
        #else
        return false
        #endif
    }
}

extension UIApplication: Assembly {
    public func assemble(container: Container) {
        container.register(RouterProtocol1.self) { resolver in
            return Router(parent: resolver)
            }.initCompleted { _, router in
                router.register(TestVC.self)
                router.register(ShowNavigationVC.self)
        }.inObjectScope(.container)
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ReaktiveBoar.setup()
        guard !UIApplication.isTesting else {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.makeKeyAndVisible()
            window.rootViewController = UIViewController()
            self.window = window

            return true
        }

        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        window.makeKeyAndVisible()

        let router = UIApplication.shared.resolver~>(RouterProtocol1.self)
        router.makeAsRootF(TestVM.self, inNavigation: false)

        return true
    }
}
