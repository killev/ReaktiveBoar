//
//  TestVM.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 5/31/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import Bond
import Swinject
import SwinjectAutoregistration

class TestVM: VCVM {

    let items: Property<[IDVM]>
    let action = PassthroughSubject<Void, Never>()
    let showVC = PassthroughSubject<Void, Never>()
    required init(resolver: Resolver) {
        let router = resolver~>RouterProtocol1.self
        items = Property([TestCellVM(resolver: resolver),
                          TestCellVM(resolver: resolver),
                          TestCellVM(resolver: resolver),
                          TestCellVM(resolver: resolver),
                          TestCellVM(resolver: resolver),
                          TestCellVM(resolver: resolver),
                          TestCellVM(resolver: resolver)] )

        super.init(resolver: resolver)
        showVC
            .bind(to: router.show(ShowNavigatiionVM.self))
            .dispose(in: bag)

        SafeSignal(just: Result<Void, NSError>(error: NSError(domain: "Test", code: 000, userInfo: nil)), after: 5)
            .safe(error: router.showError(title: "Test App", msg: { $0.localizedDescription}))
            .bind(to: action)
            .dispose(in: bag)

        SafeSignal(just: (), after: 2).map { [TestCellVM(resolver: resolver)] }
            .bind(to: items)
            .dispose(in: bag)

    }
}
