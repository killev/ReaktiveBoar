//
//  TestVC.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 5/25/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class TestVC: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var showVC: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension TestVC: VCView {

    typealias VMType = TestVM

    func advise(vm: TestVM, bag: DisposeBag) {
        vm.items.diff().bind(to: collectionView, with: resolver) { reg in
            reg.register(register: TestCell.self,
                         with: SizeInfo(width: .full(offset: 0), height: .const(500)))
        }.dispose(in: bag)
        collectionView.reactive.didReachTheBottom.observeNext {
            print("did Reach the end")
        }.dispose(in: bag)
        showVC.reactive.tap
            .bind(to: vm.showVC)
            .dispose(in: bag)
    }
}
