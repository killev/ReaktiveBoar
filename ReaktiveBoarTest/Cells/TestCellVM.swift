//
//  TestCellVM.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 5/31/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit
import Swinject
import SwinjectAutoregistration

extension ImagePickerResult {
    var image: UIImage? {
        switch self {
        case .image(let image): return image
        default: return nil
        }
    }
}

enum ImagePickerStyle: ActionType {
    case library
    case camera
    case cancel
}
extension ImagePickerStyle {
    var title: String {
       return "\(self)"
    }

    var style: UIAlertAction.Style {
        return .default
    }
}

class TestCellVM: IDVM {
    let didSelect = PassthroughSubject<Void, Never>()
    let image = Property<UIImage?>(nil)
    init(resolver: Resolver) {
        let router: RouterProtocol1 = resolver~>
        super.init(id: UUID())

        let imageSignal = didSelect.flatMapLatest {
            router.showActions(ImagePickerStyle.self, title: "Select", message: "Message")
        }.map { option -> UIImagePickerController? in
            let picker = UIImagePickerController()
            switch option {
            case .camera: picker.sourceType = .camera
            case .library: picker.sourceType = .photoLibrary
            default: return nil
            }
            return picker
        }.ignoreNils()
            .flatMapLatest(router.showPicker)
            .safe(error: router.showError(title: "", msg: { $0.localizedDescription }))
            .share()
//
        imageSignal.eraseType().bind(to: router.closeAll()).dispose(in: bag)
        imageSignal
            .map { $0.image }
            .bind(to: image).dispose(in: bag)

    }
}
