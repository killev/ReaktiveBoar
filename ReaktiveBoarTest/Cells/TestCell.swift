//
//  TestCell.swift
//  ReaktiveBoarTest
//
//  Created by Peter Ovchinnikov on 5/31/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class TestCell: UICollectionViewCell {

}

extension TestCell: Cell {
    func advise(vm: TestCellVM, bag: DisposeBag) {
        reactive.didSelect
            .bind(to: vm.didSelect)
            .dispose(in: bag)
    }

    typealias VMType = TestCellVM
}
