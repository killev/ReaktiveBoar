Pod::Spec.new do |s|

  s.name         = "ReaktiveBoar"
  s.version      = "0.61.0"
  s.summary      = "A Swift binding framework"

  s.description  = <<-DESC
                   Bond is a Swift reactive binding framework that takes binding concept to a whole new level.
                   It's simple, powerful, type-safe and multi-paradigm - just like Swift.

                   Bond is also a framework that bridges the gap between the reactive and imperative paradigms.
                   You can use it as a standalone framework to simplify your state changes with bindings and reactive data sources,
                   but you can also use it with ReactiveKit to complement your reactive data flows with bindings and
                   reactive delegates and data sources.
                   DESC

  s.homepage     = "https://gitlab.com/killev/ReaktiveBoar"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Petro Ovchynnikov" => "peter@boarlabs.xyz" }
  # s.social_media_url   = ""
  s.ios.deployment_target = "11.0"
  # s.osx.deployment_target = "11.11"
  # s.tvos.deployment_target = "9.0"
  s.source       = { :git => "https://gitlab.com/killev/ReaktiveBoar.git", :tag => "v#{s.version.to_s}" }
  s.source_files  = "ReaktiveBoar/**/*.{h,m,swift}", "Supporting Files/Bond.h"
  s.pod_target_xcconfig = { "OTHER_SWIFT_FLAGS" => "-DBUILDING_WITH_XCODE $(inherited)" }
  s.requires_arc = true
  s.swift_version = '5.0'

  s.dependency "Bond", "~> 7.0"
  s.dependency "SwinjectAutoregistration", '~> 2.6'
  s.dependency "BrightFutures", '~> 8.0'
  
end
