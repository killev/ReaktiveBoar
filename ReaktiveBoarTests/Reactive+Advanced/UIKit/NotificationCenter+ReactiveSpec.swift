//
//  NotificationCenter+ReactiveSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 6/4/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Quick
import Nimble
import ReaktiveBoar
import ReactiveKit

extension Notification.Name {
    static let testNotification = Notification.Name("testNotification")
}

class NotificationCenterReactiveSpec: QuickSpec {
    override func spec() {
        describe("NotificationCenterReactive") {
            beforeEach {
                self.bag.dispose()
            }
            it("post(name: ) should send notificaitions with value, notification(name: ) should recive") {
                let notification = Notification(name: .testNotification, object: "string")
                NotificationCenter.default.reactive
                    .notification(name: .testNotification)
                    .expectNext123([notification],
                                   expectation: QuickSpec.current.expectation(description: "Notifications"))
                    .dispose(in: self.bag)

                SafeSignal(just: "string")
                    .bind(to: NotificationCenter.default.reactive.post(name: .testNotification)).dispose(in: self.bag)

                QuickSpec.current.waitForExpectations(timeout: 1, handler: nil)
            }

            it("post(name: ) should send notificaitions with value, notification(name: ) should recive") {
                let notification = Notification(name: .testNotification, object: nil)
                NotificationCenter.default.reactive
                    .notification(name: .testNotification)
                    .expectNext123([notification],
                                   expectation: QuickSpec.current.expectation(description: "Notifications"))
                    .dispose(in: self.bag)

                SafeSignal(just: ())
                    .bind(to: NotificationCenter.default.reactive.post(name: .testNotification)).dispose(in: self.bag)

                QuickSpec.current.waitForExpectations(timeout: 1, handler: nil)
            }

            it("keyboardHeight should send notification") {

                let textField = UITextField(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
                let viewController = UIViewController()
                UIApplication.shared.keyWindow!.rootViewController = viewController
                viewController.view.addSubview(textField)

                var result: [CGFloat] = []

                NotificationCenter.default.reactive.keyboardHeight
                    .observeNext { height in
                        result.append(height)
                    }
                    .dispose(in: self.bag)

                textField.becomeFirstResponder()
                justSleep(.milliseconds(300))
                textField.resignFirstResponder()
                justSleep(.milliseconds(300))

                expect(result).toEventually(contain([0.0]))
            }
        }
    }
}
