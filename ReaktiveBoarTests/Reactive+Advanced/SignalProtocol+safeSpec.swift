//
//  SignalProtocol+safeSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Quick
import Nimble
import BrightFutures
import ReactiveKit
@testable import ReaktiveBoar

class SignalProtocolSafeSpec: QuickSpec {
    override func spec() {
        var signal: PassthroughSubject<String, TestError>!
        describe("Safe operator") {
            beforeEach {
                signal = PassthroughSubject()
            }
            it("Should ") {
                let error = PassthroughSubject<TestError?, Never>()
                let value = signal.safe(error: error)

                error.expectNext123([nil, nil, .error],
                                    expectation: QuickSpec.current.expectation(description: "For Errors"))

                value.expectNext(["1", "2"],
                                    expectation: QuickSpec.current.expectation(description: "For Values"))

                signal.send("1")
                signal.send("2")
                signal.send(completion: .failure(.error))

                QuickSpec.current.waitForExpectations(timeout: 2, handler: nil)

            }
        }

    }
}
