//
//  SignalProtocol+AdvancedOperatorsSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/22/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Quick
import Nimble
import ReactiveKit
import ReaktiveBoar

class SignalProtocolAdvancedOperatorsSpec: QuickSpec {
    override func spec() {
        describe("SignalProtocol") {
            describe("reoverNil") {
                it("should convert to recovering value if nil") {
                    //QuickSpec.current.expectation(description: "Observing")
                    SafeSignal<String?>(just: nil).recoverNils("default")
                        .expectNext(["default"])
                    //QuickSpec.current.waitForExpectations(timeout: 1, handler: nil)
                }
                it("shouldn't convert to recovering value if not nil") {
                    //QuickSpec.current.expectation(description: "Observing")
                    SafeSignal<String?>(just: "value").recoverNils("default")
                        .expectNext(["value"])
                    //QuickSpec.current.waitForExpectations(timeout: 1, handler: nil)
                }
            }
            describe("flatMapLates with working") {
               it("It should raise true when starts and false when resives first event") {
                    let originalStream = PassthroughSubject<Int, Never>()
                    let workigSignal = PassthroughSubject<Bool, Never>()
                    let result = PassthroughSubject<String, Never>()
                    workigSignal.expectNext123([false, true, false],
                                            expectation: QuickSpec.current
                                                .expectation(description: "For working stream"))

                    result.expectNext123(["2", "3"],
                                            expectation: QuickSpec.current
                                                .expectation(description: "For result Stream"))
                originalStream.flatMapLatest(working: workigSignal) {
                        return SafeSignal.init(sequence: ["\($0 + 1)", "\($0 + 2)"], interval: 0.1)
                        }.bind(to: result).dispose(in: self.bag)
                    originalStream.send(1)

                    QuickSpec.current.waitForExpectations(timeout: 5, handler: nil)
                    self.bag.dispose()
                    originalStream.send(3)//We shouldn't take any event anymore
                }

            }
        }
    }
}
