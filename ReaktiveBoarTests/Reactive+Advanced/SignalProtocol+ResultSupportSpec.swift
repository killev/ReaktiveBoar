//
//  SignalProtocol+ResultSupportSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 6/4/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

// swiftlint:disable function_body_length

import Quick
import Nimble
import ReactiveKit
import ReaktiveBoar

class SignalProtocolResultSupportSpec: QuickSpec {
    override func spec() {
        describe("SignalProtocol+Result Payload") {
            beforeEach {
                self.bag.dispose()
            }
            describe("safe(error: )") {

                it("should drop errors and pass value next") {
                    let err = NSError(domain: "Test", code: 0, userInfo: nil)

                    let error = PassthroughSubject<NSError?, Never>()
                    let value = PassthroughSubject<String, Never>()
                    let signal = PassthroughSubject<Result<String, NSError>, Never>()
                    signal.safe(error: error)
                        .bind(to: value)
                        .dispose(in: self.bag)

                    error.expectNext123([nil, err, nil],
                                     expectation: QuickSpec.current.expectation(description: "For Errors"))

                    value.expectNext123(["String", "String1"],
                                     expectation: QuickSpec.current.expectation(description: "For Values"))

                    signal.send(Result(value: "String"))
                    signal.send(Result(error: err))
                    signal.send(Result(value: "String1"))
                    self.bag.dispose()
                    signal.send(Result(value: "String3")) // should be ignored

                    QuickSpec.current.waitForExpectations(timeout: 2, handler: nil)
                }
            }
            describe("splitBind(error:, value:)") {

                it("should drop errors and pass value next") {
                    let err = NSError(domain: "Test", code: 0, userInfo: nil)

                    let error = PassthroughSubject<NSError?, Never>()
                    let value = PassthroughSubject<String, Never>()
                    let signal = PassthroughSubject<Result<String, NSError>, Never>()
                    signal.splitBind(error: error, value: value)
                        .dispose(in: self.bag)

                    error.expectNext123([nil, err, nil],
                                        expectation: QuickSpec.current.expectation(description: "For Errors"))

                    value.expectNext123(["String", "String1"],
                                        expectation: QuickSpec.current.expectation(description: "For Values"))

                    signal.send(Result(value: "String"))
                    signal.send(Result(error: err))
                    signal.send(Result(value: "String1"))
                    self.bag.dispose()
                    signal.send(Result(value: "String3")) // should be ignored

                    QuickSpec.current.waitForExpectations(timeout: 2, handler: nil)
                }
            }
            describe("(error:, value:)") {

                it("should drop errors and pass value next") {
                    let err = NSError(domain: "Test", code: 0, userInfo: nil)

                    let error = PassthroughSubject<NSError?, Never>()
                    let value = PassthroughSubject<String, Never>()
                    let signal = PassthroughSubject<Result<String, NSError>, Never>()
                    signal.splitBind(error: error, value: value)
                        .dispose(in: self.bag)

                    error.expectNext123([nil, err, nil],
                                        expectation: QuickSpec.current.expectation(description: "For Errors"))

                    value.expectNext123(["String", "String1"],
                                        expectation: QuickSpec.current.expectation(description: "For Values"))

                    signal.send(Result(value: "String"))
                    signal.send(Result(error: err))
                    signal.send(Result(value: "String1"))
                    self.bag.dispose()
                    signal.send(Result(value: "String3")) // should be ignored

                    QuickSpec.current.waitForExpectations(timeout: 2, handler: nil)
                }
            }

        }
    }
}
