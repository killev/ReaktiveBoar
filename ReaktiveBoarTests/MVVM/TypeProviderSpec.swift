//
//  TypeProviderSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/18/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Quick
import Nimble

@testable import ReaktiveBoar

class TypeProvider: TypeProviderProtocol {

}

class TypeProviderHierarchy: TypeProvider {

}
struct NonTypeProvider {

}

class TypeProviderSpec: QuickSpec {
    override func spec() {

        describe("TypeProvider protocol") {
            it("should provide className and T.className and they're equal") {
                let typeProvider = TypeProvider()
                expect(typeProvider.className).to(equal(TypeProvider.className))
            }
        }

        describe("className(data:)") {
            it("should use className if it's a TypeProviderProtocol") {
                let typeProvider = TypeProvider()
                expect(className(data: typeProvider)).to(equal(typeProvider.className))
            }
            it("should use type(of:) if it's not TypeProviderProtocol") {
                let typeProvider = NonTypeProvider()
                expect(className(data: typeProvider)).to(equal("NonTypeProvider"))
            }
        }

        describe("className(type:)") {
            it("should use className if it's a TypeProviderProtocol") {
                expect(className(type: TypeProvider.self)).to(equal(TypeProvider.className))
            }
            it("should use type(of:) if it's not TypeProviderProtocol") {
                expect(className(type: NonTypeProvider.self)).to(equal("NonTypeProvider"))
            }
        }

        describe("heirarchy") {

            it("provide herarchy for an instance of TypeProviderHierarchy") {
                let hierarchyObject = TypeProviderHierarchy()
                let hier1 = hier(data: hierarchyObject)
                expect(hier1)
                    .to(equal([TypeProviderHierarchy.className,
                               TypeProvider.className]))
            }
            it("provide herarchy for an an instance of NonTypeProvider") {
                let hierarchyObject = NonTypeProvider()
                let hier1 = hier(data: hierarchyObject)
                let name = className(type: NonTypeProvider.self)
                expect(hier1)
                    .to(equal([name]))
            }
        }
    }
}
