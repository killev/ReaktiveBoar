//
//  UIViewController+ParentContainerSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Foundation

import Quick
import Nimble
import Swinject

@testable import ReaktiveBoar

class ViewControllerParentContainerSpec: QuickSpec {
    override func spec() {

        describe("ViewController parent container") {
            UIViewController.setup()
            it("should get nil no other container in hierarchy") {
                let viewController = UIViewController()
                expect(viewController.parentContainer())
                    .to(beNil())
            }
            it("should get from navigation controller") {
                let viewController = UIViewController()
                let nav = UINavigationController(rootViewController: viewController)
                nav.container = Container()
                expect(viewController.parentContainer())
                    .to(be(nav.container))
            }
            it("should get container from previous in hierarchy") {
                let nav = UINavigationController(rootViewController: UIViewController())
                nav.pushViewController(UIViewController(), animated: false)
                nav.pushViewController(UIViewController(), animated: false)

                expect(nav.viewControllers.count).to(be(3))
                expect(nav.topViewController).to(be( nav.viewControllers[2]))

                nav.viewControllers[0].container = Container()
                expect(nav.viewControllers[2].parentContainer())
                    .to(be(nav.viewControllers[0].container ))
                expect(nav.viewControllers[1].parentContainer())
                    .to(be(nav.viewControllers[0].container ))
            }

            it("should get container from tab controller if no previous in hierarchy") {

                let tab = UIStoryboard(name: "MainTabVC",
                                       bundle: Bundle(for: ViewControllerParentContainerSpec.self))
                    .instantiateInitialViewController() as! UITabBarController //swiftlint:disable:this force_cast

                expect(tab.children.count).to(be(2))

                tab.container = Container()
                let nav = tab.children[0] as! UINavigationController //swiftlint:disable:this force_cast
                let vc = nav.topViewController!
                expect(vc.parentContainer()).to(be(tab.container))
            }

            it("should get container from presentation controller if no previous in hierarchy") {

                let one = UIViewController()
                UIApplication.shared.keyWindow!.rootViewController = one
                one.container = Container()
                let two = UIViewController()
                one.present(two, animated: false, completion: nil)
                expect(two.parentContainer()).to(be(one.container))
            }
        }
    }
}
