//
//  ConstraintBasedCollectionViewSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 6/12/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import XCTest
import ReactiveKit
import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import Quick
import Nimble

class ConstraintBasedCollectionViewSpec: QuickSpec {

    override func spec() {
        describe("constraint based collection view datasource binding") {

            var collectionView: TestCollectionView!
            var bag: DisposeBag!
            beforeEach {
                bag = DisposeBag()
                collectionView = TestCollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100),
                                                    collectionViewLayout: UICollectionViewFlowLayout())

            }

            it("initializes cells correctly") {
                let container = Container()
                container.autoregister(TestStringCellVM.self, initializer: TestStringCellVM.init)

                let data = Property<[Identifiable]>(["item1",
                                                     "item2",
                                                     TestVMCellVM(),
                                                     TestVMCellVMHier()])

                let estimatedItemSize = CGSize(width: 1, height: 1)

                _ = data.bind(to: collectionView,
                              estimatedItemSize: estimatedItemSize,
                          with: container) { registrator in

                            registrator.register(for: String.self,
                                                 register: TestStringCell.self)

                            registrator.register(register: TestVMCell.self)
                }.dispose(in: bag)

                expect(
                    (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.estimatedItemSize)
                    .to(equal(estimatedItemSize))

                expect(collectionView.numberOfSections).to(be(1))
                expect(collectionView.numberOfItems(inSection: 0)).to(be(data.value.count))

                let stringCell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0))
                expect(stringCell).notTo(beNil())
                expect(stringCell).to(beAnInstanceOf(TestStringCell.self))

                let idCell = collectionView.cellForItem(at: IndexPath(row: 2, section: 0))
                expect(idCell).notTo(beNil())
                expect(idCell).to(beAnInstanceOf(TestVMCell.self))
            }

        }
    }
}
