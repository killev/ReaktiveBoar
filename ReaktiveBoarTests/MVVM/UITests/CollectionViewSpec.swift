//
//  CollectionViewTests.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/17/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

// swiftlint:disable function_body_length

import XCTest
import ReactiveKit
import Swinject
import SwinjectAutoregistration
import ReaktiveBoar
import Quick
import Nimble

class TestCollectionView: UICollectionView {
    override func cellForItem(at indexPath: IndexPath) -> UICollectionViewCell? {
        return dataSource?.collectionView(self, cellForItemAt: indexPath)
    }
    var reloadDataCount: Int = 0
    override func reloadData() {
        reloadDataCount += 1
        super.reloadData()
    }
}

class CollectionViewSpec: QuickSpec {

    override func spec() {
        describe("collection view datasource binding") {

            var collectionView: TestCollectionView!
            var bag: DisposeBag!
            beforeEach {
                bag = DisposeBag()
                collectionView = TestCollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100),
                                                    collectionViewLayout: UICollectionViewFlowLayout())

            }

            it("initializes cells correctly") {
                let container = Container()
                container.autoregister(TestStringCellVM.self, initializer: TestStringCellVM.init)

                let data = Property<[Identifiable]>(["item1",
                                                     "item2",
                                                     TestVMCellVM(),
                                                     TestVMCellVMHier()])

                data.bind(to: collectionView, with: container) { registrator in
                    registrator.register(for: String.self,
                                         register: TestStringCell.self,
                                         with: SizeInfo())

                    registrator.register(register: TestVMCell.self,
                                        with: SizeInfo())

                }.dispose(in: bag)

                expect(collectionView.numberOfSections).to(be(1))
                expect(collectionView.numberOfItems(inSection: 0)).to(be(data.value.count))

                let stringCell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0))
                expect(stringCell).notTo(beNil())
                expect(stringCell).to(beAnInstanceOf(TestStringCell.self))

                let idCell = collectionView.cellForItem(at: IndexPath(row: 2, section: 0))
                expect(idCell).notTo(beNil())
                expect(idCell).to(beAnInstanceOf(TestVMCell.self))

            }

            it("configure cells if configurable") {
                let container = Container()
                let data = Property<[Identifiable]>([TestVMCellVM()])

                data.bind(to: collectionView, with: container) { registrator in
                    registrator.register(register: TestVMConfigurableCell.self,
                                         with: SizeInfo(),
                                         config: "Test")

                    }.dispose(in: bag)

                expect(collectionView.numberOfSections).to(be(1))
                expect(collectionView.numberOfItems(inSection: 0)).to(be(data.value.count))

                let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0))
                expect(cell).notTo(beNil())
                expect(cell).to(beAnInstanceOf(TestVMConfigurableCell.self))

                // swiftlint:disable:next force_cast
                expect((cell as! TestVMConfigurableCell).config).to(be("Test"))
            }

            it("configure cells if configurable") {
                let container = Container()
                let data = Property<[Identifiable]>([TestVMCellVM()])

                data.bind(to: collectionView, with: container) { registrator in
                    registrator.register(register: TestVMConfigurableCell.self,
                                         with: SizeInfo(),
                                         config: "Test")

                    }.dispose(in: bag)

                expect(collectionView.numberOfSections).to(be(1))
                expect(collectionView.numberOfItems(inSection: 0)).to(be(data.value.count))

                let cell = collectionView.cellForItem(at: IndexPath(row: 0, section: 0))
                expect(cell).notTo(beNil())
                expect(cell).to(beAnInstanceOf(TestVMConfigurableCell.self))

                // swiftlint:disable:next force_cast
                expect((cell as! TestVMConfigurableCell).config).to(be("Test"))
            }

            describe("UIScrollView + Reactive functionality") {
                it("Should invoke didReachTheBottom if you scroll to the bottom of collaction vew") {

                    let data = Property<[Identifiable]>([TestVMCellVM(),
                                                         TestVMCellVM(),
                                                         TestVMCellVM()])
                    let container = Container()
                    data.bind(to: collectionView, with: container) { registrator in
                        registrator.register(register: TestVMConfigurableCell.self,
                                             with: SizeInfo(width: .full(offset: 0), height: .const(500)))

                        }.dispose(in: bag)

                    collectionView.scrollToBottom(animated: false)
                    print(collectionView.contentSize)
                    print(collectionView.contentOffset)
                    justSleep(.milliseconds(100))
                    waitUntil { done in
                        _ = collectionView.reactive.didReachTheBottom.observeNext {
                            done()
                        }
                    }
                }
            }

//            it("It should updata collectionView once only if no changes") {
//                let container = Container()
//                container.autoregister(TestStringCellVM.self, initializer: TestStringCellVM.init)
//
//                let data = Property<[IDVM]>([TestStringCellVM(str: "Test")])
//
//                data.diff().bind(to: collectionView, with: container) { registrator in
//                    registrator.register(register: TestStringCell.self,
//                                         with: SizeInfo())
//                }.dispose(in: bag)
//                expect(collectionView.reloadDataCount).to(be(1))
//                data.value = [TestStringCellVM(str: "Test")]
//                expect(collectionView.reloadDataCount).to(be(1))
//                data.next([TestStringCellVM(str: "Test"),
//                           TestStringCellVM(str: "Test2")])
//
//                expect(collectionView.numberOfItems(inSection: 0)).to(equal(data.value.count))
//
//            }

//            it("It should updata collectionView once only if no changes no diff") {
//                let container = Container()
//                container.autoregister(TestStringCellVM.self, initializer: TestStringCellVM.init)
//
//                let data = Property<[IDVM]>([TestStringCellVM(str: "Test")])
//
//                data.bind(to: collectionView, with: container) { registrator in
//                    registrator.register(register: TestStringCell.self,
//                                         with: SizeInfo())
//                    }.dispose(in: bag)
//                expect(collectionView.reloadDataCount).to(be(1))
//                data.value = [TestStringCellVM(str: "Test")]
//                expect(collectionView.reloadDataCount).to(be(1))
//                data.next([TestStringCellVM(str: "Test"),
//                           TestStringCellVM(str: "Test2")])
//
//                expect(collectionView.numberOfItems(inSection: 0)).to(equal(data.value.count))
//
//            }

        }
    }
}
