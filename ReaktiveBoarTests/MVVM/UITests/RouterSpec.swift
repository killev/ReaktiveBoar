//
//  RouterSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/21/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

// swiftlint:disable function_body_length

import Quick
import Nimble
import Swinject

@testable import ReaktiveBoar
import ReactiveKit
import SwinjectAutoregistration

class RouterSpec: QuickSpec {

    override func spec() {
        describe("Router") {
            var router: Router!
            var rootController: UIViewController!
            beforeEach {
                router = Router()
                router.register(TestController.self)
                rootController = UIViewController()
                UIApplication.shared.keyWindow?.rootViewController = rootController
            }

            it("Sould present/dismiss controller if no navigationController ") {

                waitUntil(timeout: 5) { done in
                    router.showF(TestControllerVM.self, data: (), animated: true)
                        .onSuccess { vc in
                            expect(vc).to(beAnInstanceOf(TestController.self))
                            expect(vc.navigationController).notTo(beNil())
                            expect(vc.navigationController!.presentingViewController)
                                .to(be(rootController))

                            expect(Router.topViewController()).to(be(vc))

                        }.flatMap { vc in
                            router.closeF(animated: false).map {_ in vc }
                        }.onSuccess { vc in
                            expect(vc.navigationController).to(beNil())
                            expect(vc).notTo(be(Router.topViewController()))

                        }.onFailure { err in
                            fail("\(err)")
                        }.onComplete { _ in
                            done()
                    }
                }
            }

            it("CloseAll should dismiss until navigationController") {

               waitUntil(timeout: 5) { done in
                    router.showF(TestControllerVM.self, animated: true)
                        .flatMap {_ in router.showF(TestControllerVM.self, animated: true) }
                        .onSuccess { vc in
                            expect(vc).to(beAnInstanceOf(TestController.self))
                            expect(vc.navigationController).notTo(beNil())
                            expect(vc.navigationController!.presentingViewController)
                                .to(be(rootController))

                        }.flatMap { vc in
                            router.closeAllF(animated: false).map {_ in vc }
                        }.onSuccess { vc in
                            expect(vc.navigationController).to(beNil())
                            expect(vc).notTo(be(Router.topViewController()))

                        }.onFailure { err in
                            fail("\(err)")
                        }.onComplete { _ in
                            done()
                    }
                }
            }

            it("show should pass container from topController") {
                let original = "This is a string"
                let container = Container()
                container.register(value: original)

                rootController.container = container

                waitUntil(timeout: 5) { done in
                    router.showF(TestControllerVM.self, animated: true)
                        .onSuccess { vc in
                            let str = vc.resolver.resolve(String.self)
                            expect(str).to(equal(original))
                        }.onFailure { err in
                            fail("\(err)")
                        }.onComplete { _ in
                            done()
                    }
                }
            }

            it("show should pass container from router in no container in the topController") {
                let original = "This is a string"
                let container = Container()
                container.register(value: original)
                let router = Router(parent: container)
                router.register(TestController.self)

                waitUntil(timeout: 5) { done in
                    router.showF(TestControllerVM.self, animated: true)
                        .onSuccess { vc in
                            let str = vc.resolver.resolve(String.self)
                            expect(str).to(equal(original))
                        }.onFailure { err in
                            fail("\(err)")
                        }.onComplete { _ in
                            done()
                    }
                }
            }
            it("Sould pass data via container to viewModel ") {

                waitUntil(timeout: 5) { done in
                    let original = "This is a string"
                    router.showF(TestControllerVM.self, data: original, animated: true)
                        .onSuccess { vc in
                            expect(vc).to(beAnInstanceOf(TestController.self))
                            // swiftlint:disable:next force_cast
                            let testVC = vc as! TestController
                            expect(testVC.str).to(equal(original))
                        }.onFailure { err in
                            fail("\(err)")
                        }.onComplete { _ in
                            done()
                    }
                }
            }

            it("Sould showAsRoot with navigation") {
                let original = "This is a string"
                let vc =  router.makeAsRootF(TestControllerVM.self,
                                            inNavigation: true,
                                            data: original)
                waitUntil { done in
                    vc.onSuccess { vc in
                        expect(vc.navigationController)
                            .notTo(beNil())
                        done()
                    }
                }
            }

            it("Sould showAsRoot without navigation") {
                let original = "This is a string"
                let vc =  router.makeAsRootF(TestControllerVM.self,
                                            inNavigation: false,
                                            data: original)
                waitUntil { done in
                    vc.onSuccess { vc in
                        expect(vc.navigationController)
                            .to(beNil())
                        done()
                    }
                }
            }
        }
    }
}
