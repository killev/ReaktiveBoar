//
//  ViewSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 5/17/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Quick
import Nimble
import ReaktiveBoar
import ReactiveKit

class TestReuseView: UIView, ReuseView {
    func advise(vm: TestVMCellVM, bag: DisposeBag) {
       vm.color
        .bind(to: reactive.backgroundColor)
        .dispose(in: bag)
    }

    typealias VMType = TestVMCellVM

}

class ViewSpec: QuickSpec {
    override func spec() {
        describe("ReuseView") {
            it("should advise unadvise during lifecycle") {
                let view = TestReuseView()
                let vm = TestVMCellVM()
                view.vm = vm
                expect(view.vm).notTo(beNil())
                expect(view.backgroundColor).to(be(UIColor.black))
                vm.color.send(.white)
                expect(view.backgroundColor).to(be(UIColor.white))
                view.vm = nil
                vm.color.send(.yellow)
                expect(view.backgroundColor).to(be(UIColor.white))
            }
        }
    }
}
