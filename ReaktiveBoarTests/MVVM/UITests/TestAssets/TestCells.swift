//
//  TestCells.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/19/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReaktiveBoar
import ReactiveKit

class TestStringCellVM: IDVM {
    init(str: String) {
        super.init(id: str)
    }
}

class TestStringCell: UICollectionViewCell, Cell {

    func advise(vm: TestStringCellVM, bag: DisposeBag) {

    }

    typealias VMType = TestStringCellVM
}

class TestVMCellVM: VMProtocol, Identifiable {
    var id: String { return UUID().uuidString }
    let color = Property<UIColor>(.black)
    init() {

    }
}

class TestVMCellVMHier: TestVMCellVM {

    override init() {

    }
}

class TestVMCell: UICollectionViewCell, Cell {
    func advise(vm: TestVMCellVM, bag: DisposeBag) {

    }

    typealias VMType = TestVMCellVM
}

class TestVMConfigurableCell: UICollectionViewCell, Cell, Configurable {

    var config: String = ""
    func configure(with config: String) {
        self.config = config
    }

    typealias Config = String

    func advise(vm: TestVMCellVM, bag: DisposeBag) {

    }

    typealias VMType = TestVMCellVM
}
