//
//  ViewControllerSpec.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/21/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

//swiftlint:disable

import Quick
import Nimble
import Swinject

@testable import ReaktiveBoar

class ViewControllerSpec: QuickSpec {
    // swiftlint:disable:next function_body_length
    override func spec() {

        describe("ViewController") {
            UIViewController.setup()

            var viewController: TestController!
            var container: Container!
            beforeEach {
                container = Container()

                let router = Router(parent: container)
                router.register(TestController.self)

                router.showF(TestControllerVM.self, animated: false)
                    .onSuccess { vc in
                        viewController = vc as? TestController
                }

                justSleep(.seconds(1))
                _ = viewController?.view
            }

            it("Should advise the vm to controller during lifecycle") {
                viewController?.beginAppearanceTransition(true, animated: false)
                viewController?.endAppearanceTransition()

                expect(viewController?.advised).to(beTruthy())

                viewController?.viewWillDisappear(false)
                viewController?.viewDidDisappear(false)
            }
        }
        describe("ViewController Standalone") {
            it("Should should not create container") {
                expect(UIViewController().container).to(beNil())
            }

            it("Standalone vc should use parent Container to be advised") {

                let testVC = UIStoryboard(name: TestController.className,
                                       bundle: Bundle(for: TestController.self))
                    .instantiateInitialViewController() as! TestController //swiftlint:disable:this force_cast
                let navVC = UINavigationController(rootViewController: testVC)

                let str = "This is a string"
                let container = Container()

                container.register(value: str)
                navVC.container = container

                navVC.beginAppearanceTransition(true, animated: false)
                navVC.endAppearanceTransition()
                testVC.beginAppearanceTransition(true, animated: false)
                testVC.endAppearanceTransition()

                expect(testVC.advised).to(beTrue())
                expect(testVC.str).to(equal(str))

                testVC.viewWillDisappear(false)
                testVC.viewDidDisappear(false)

                navVC.viewWillDisappear(false)
                navVC.viewDidDisappear(false)
            }
        }

    }
}
