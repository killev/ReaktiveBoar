//
//  TryCatch.h
//  ReaktiveBoar
//
//  Created by Peter Ovchinnikov on 6/12/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TryCatch : NSObject

/**
 Provides try catch functionality for swift by wrapping around Objective-C
 */

+ (void)try:(__attribute__((noescape))  void(^ _Nullable)(void))try catch:(__attribute__((noescape)) void(^ _Nullable)(NSException*exception))catch finally:(__attribute__((noescape)) void(^ _Nullable)(void))finally;
+ (void)throwString:(NSString*)s;
+ (void)throwException:(NSException*)e;
@end

NS_ASSUME_NONNULL_END
