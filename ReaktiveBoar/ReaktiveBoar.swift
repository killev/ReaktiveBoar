//
//  ReaktiveBoar.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 8/30/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Swinject

public struct ReaktiveBoar {

    private static var _setup: Void = {
        if let assembly = UIApplication.shared as? (Assembly & ContainerProvider) {

            if assembly.container == nil {
                assembly.container = Container()
            }

            assembly.assemble(container: assembly.container!)
            assembly.loaded(resolver: assembly.container!)
        }

        UIViewController.setup()
    }()
    public static func setup() {
        _ = _setup
    }
}
