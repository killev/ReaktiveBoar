//
//  ContainerProvider.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 6/2/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Swinject

protocol ContainerProvider: class {
    var container: Container? { get set }
}

public protocol ResolverProvider {
    var resolver: Resolver { get }
}
var containerKey = ""
var defContainerKey = ""

extension ContainerProvider {
    var container: Container? {
        get {
            return Associated<Self,
                Container?>(self, &containerKey)
                .value(initial: { nil })
        }
        set {
            Associated<Self,
                Container?>(self, &containerKey)
                .value(new: newValue)
        }
    }
}
