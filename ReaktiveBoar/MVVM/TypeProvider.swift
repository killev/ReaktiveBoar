//
//  TypeProvider.swift
//  ReaktiveBoar
//
//  Created by Peter Ovchinnikov on 3/18/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

public protocol TypeProviderProtocol {
    var className: String { get }
    var hierarchy: [String] { get }
    //static var className: String { get }
}
extension TypeProviderProtocol {
    public var className: String {
        return "\(Swift.type(of: self))"
    }

    public var hierarchy: [String] {
        var res = [String]()
        var miror: Mirror?  = Mirror(reflecting: self)
        while miror != nil {
            res.append("\(miror!.subjectType)")
            miror = miror?.superclassMirror
        }
        return res
    }

    public static var className: String {
        return "\(Self.self)"
    }
}

public func className<T>(data: T) -> String {
    if let typeProvider = data as? TypeProviderProtocol {
        return typeProvider.className
    }
    return "\(type(of: data))"
}

public func className<T>(type: T.Type) -> String {
    return "\(type)"
}

public func hier<T>(data: T) -> [String] {
    if let typeProvider = data as? TypeProviderProtocol {
        return typeProvider.hierarchy
    }
    var res = [String]()
    var miror: Mirror?  = Mirror(reflecting: data)
    while miror != nil {
        res.append("\(miror!.subjectType)")
        miror = miror?.superclassMirror
    }
    return res
}

extension String: TypeProviderProtocol { }
