//
//  VC.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/12/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import UIKit
import ReactiveKit
import Swinject
import SwinjectAutoregistration

public protocol VCLifeCycle: class {
    func viewDidLoadImpl()
    func viewWillAppearImpl()
}

private var vmKey = ""
public extension VCLifeCycle where Self: UIViewController, Self: VCView, Self: Assembly {

    func viewDidLoadImpl() {

        let cont = container ?? parentContainer()?.push() ?? Container()
        assemble(container: cont)
        loaded(resolver: cont)
        container = cont
        let vm = VMType(resolver: resolver)
        cont.register(value: vm).inObjectScope(.container)
        advise(vm: vm, bag: reactive.bag)
    }

    func viewWillAppearImpl() {
        //globalAdvise(vm: vm, bag: bag)
    }
}

public extension VCLifeCycle where Self: UIViewController, Self: VCView {

    func viewDidLoadImpl() {
        let cont = container ?? parentContainer()?.push() ?? Container()
        container = cont
        let vm = VMType(resolver: resolver)
        cont
            .register(value: vm)
            .inObjectScope(.container)
        advise(vm: vm, bag: reactive.bag)
    }

    func viewWillAppearImpl() {
        //globalAdvise(vm: vm, bag: bag)
    }
}

public protocol View: class {
    associatedtype VMType: VMProtocol
    func advise(vm: VMType, bag: DisposeBag)
    func unadvise(bag: DisposeBag)
}

public extension View {
    func unadvise(bag: DisposeBag) { }
}

public protocol VCVMProtocol: VMProtocol {
    init(resolver: Resolver)
}
public protocol VCView: View, VCLifeCycle where VMType: VCVMProtocol { }

extension UIViewController {

    private static let swizzle: Void = {
        func registerSwizzle(_ originalSelector: Selector, swizzledSelector: Selector, clazz: AnyClass) {
            let originalMethod = class_getInstanceMethod(clazz, originalSelector)
            let swizzledMethod = class_getInstanceMethod(clazz, swizzledSelector)

            let didAddMethod = class_addMethod(clazz,
                                               originalSelector,
                                               method_getImplementation(swizzledMethod!),
                                               method_getTypeEncoding(swizzledMethod!))

            if didAddMethod {
                class_replaceMethod(clazz,
                                    swizzledSelector,
                                    method_getImplementation(originalMethod!),
                                    method_getTypeEncoding(originalMethod!))
            } else {
                method_exchangeImplementations(originalMethod!, swizzledMethod!)
            }
        }
        registerSwizzle(#selector(UIViewController.viewDidAppear(_:)),
                        swizzledSelector: #selector(UIViewController.nsh_viewDidAppear(_:)),
                        clazz: UIViewController.self)
        registerSwizzle(#selector(UIViewController.viewWillAppear(_:)),
                        swizzledSelector: #selector(UIViewController.nsh_viewWillAppear(_:)),
                        clazz: UIViewController.self)
        registerSwizzle(#selector(UIViewController.viewDidDisappear(_:)),
                        swizzledSelector: #selector(UIViewController.nsh_viewDidDisappear(_:)),
                        clazz: UIViewController.self)
        registerSwizzle(#selector(UIViewController.viewWillDisappear(_:)),
                        swizzledSelector: #selector(UIViewController.nsh_viewWillDisappear(_:)),
                        clazz: UIViewController.self)
        registerSwizzle(#selector(UIViewController.viewDidLoad),
                        swizzledSelector: #selector(UIViewController.nsh_viewDidLoad),
                        clazz: UIViewController.self)
    }()

    internal static func setup() {
        // make sure this isn't a subclass
        if self !== UIViewController.self {
            return
        }
        UIViewController.swizzle
    }

    @objc func nsh_viewDidLoad() {
        self.nsh_viewDidLoad()
        if let liveCycle = self as? VCLifeCycle {
            liveCycle.viewDidLoadImpl()
        }
    }
    @objc func nsh_viewDidAppear(_ animated: Bool) {
        self.nsh_viewDidAppear(animated)

    }
    @objc  func nsh_viewWillAppear(_ animated: Bool) {
        self.nsh_viewWillAppear(animated)
        if let liveCycle = self as? VCLifeCycle {
            liveCycle.viewWillAppearImpl()
        }
    }
    @objc func nsh_viewWillDisappear(_ animated: Bool) {
        self.nsh_viewWillDisappear(animated)
    }
    @objc func nsh_viewDidDisappear(_ animated: Bool) {
        self.nsh_viewDidDisappear(animated)
    }
}

extension UIViewController: TypeProviderProtocol { }
