//
//  UIViewController+ContainerProvider.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Swinject

extension Resolver {
    func push() -> Container {
        return Container(parent: self as? Container)
    }
}

extension UIViewController: ContainerProvider, ResolverProvider {
    public var resolver: Resolver {
        return container ?? Container()
    }
}

extension UIApplication: ContainerProvider, ResolverProvider {
    public var resolver: Resolver {
        return container ?? Container()
    }
}

extension UIViewController {
    func parentContainer() -> Container? {

        if let nav = navigationController {
            if let index = nav.viewControllers.firstIndex(of: self) {
                let container = nav.viewControllers.map { $0.container }
                    .dropLast(nav.viewControllers.count - index)
                        .reversed().first { $0 != nil }
                if container != nil && container! != nil {
                    return container!
                }
            }
        }

        if navigationController?.container != nil {
            return navigationController?.container
        }

        if let presenting = presentingViewController,
            let container = presenting.container {
            return container
        }

        if let tab = tabBarController, let container = tab.container {
            return container
        }
        return nil
    }
}
