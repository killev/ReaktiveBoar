//
//  VM.swift
//  AlamofireCoolCousinTests
//
//  Created by Peter Ovchinnikov on 8/7/16.
//  Copyright © 2016 Peter Ovchinnikov. All rights reserved.
//

import Swinject

public protocol VMProtocol: class, TypeProviderProtocol { }

open class VM: NSObject, VMProtocol {
    public override init() {
        super.init()
    }
}
open class VCVM: VM, VCVMProtocol {
    public required init(resolver: Resolver) {

    }
    deinit {
        print("VCVM deinit")
    }
}
