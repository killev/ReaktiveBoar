//
//  Configurable.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 5/19/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

public protocol Configurable {
    associatedtype Config
    func configure(with: Config)
}
