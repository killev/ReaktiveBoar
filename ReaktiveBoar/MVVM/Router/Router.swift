//
//  Router.swift
//  ReaktiveBoar
//
//  Created by Peter Ovchinnikov on 3/13/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Bond
import ReactiveKit
import BrightFutures
import Swinject

internal struct VCRegInfo1 {
    let create: () -> UIViewController
}

public class Router: RouterProtocol1, DisposeBagProvider, BindingExecutionContextProvider {

    public func showPicker<VC>(vc: VC) -> Signal<VC.Option, NSError> where VC: UIViewController, VC: PickerController {

        self.showVCF(vc: vc, animated: true)

        let res = vc.result.mapError({ _ -> NSError in })
        return res
    }

    public let bindingExecutionContext: ReactiveKit.ExecutionContext = .immediateOnMain
    public let bag = DisposeBag()

    let container: Container
    public init(parent: Resolver) {
        container = parent.push()
    }
    public init() {
        container = Container()
    }

    var infos = [String: VCRegInfo1]()
}
extension Router {
    @discardableResult
    public func makeAsRootF<VM: VCVMProtocol, Data>(_ vmType: VM.Type,
                                                    inNavigation: Bool,
                                                    data: Data) -> Future<UIViewController, NSError> {
        let name = className(type: vmType)
        guard let info = infos[name] else {
            fatalError("No registered ViewController for model: \(name)")
        }

        let cont = container.push()
        cont.register(value: data)
        let vc = info.create()
        return makeAsRootF(vc, inNavigation: inNavigation, data: data)
    }

    public func makeAsRootF<VC: UIViewController, Data>(_ vc: VC,
                                                        inNavigation: Bool,
                                                        data: Data) -> Future<UIViewController, NSError> {

        guard let window = UIApplication.shared.keyWindow else {
            return Future(error: BoarError.noKeyWindow)
        }

        let cont = container.push()
        cont.register(value: data)
        vc.container = cont

        let promise = Promise<UIViewController, NSError>()
        UIView.transition(with: window,
                          duration: 0.5,
                          options: [UIView.AnimationOptions.transitionFlipFromLeft,
                                    UIView.AnimationOptions.allowAnimatedContent],
                          animations: {
                            let rootVC = inNavigation ? UINavigationController(rootViewController: vc) : vc

                            window.rootViewController = rootVC

        }, completion: { res in
            if res {
                promise.success(vc)
            } else {
                promise.failure(BoarError.animationFailed("Unknown"))
            }
        })
        return promise.future

    }

    public func register<VC>(_ vcType: VC.Type)
        where VC: UIViewController, VC: VCView {
            infos[VC.VMType.className] = VCRegInfo1(create: {
                let bundle = Bundle(for: vcType)
                let vc = UIStoryboard(name: VC.className, bundle: bundle)
                    // swiftlint:disable:next force_cast
                    .instantiateInitialViewController() as! VC
                return vc
            })
    }

    @discardableResult
    public func showF<T: VCVMProtocol>(_ vmType: T.Type,
                                       animated flag: Bool) -> Future<UIViewController, NSError> {
        return showF(vmType, data: (), animated: flag)

    }

    @discardableResult
    public func showF<T: VCVMProtocol, Data>(_ vmType: T.Type,
                                             data: Data,
                                             animated flag: Bool) -> Future<UIViewController, NSError> {

        let name = className(type: vmType)
        guard let info = infos[name] else {
            fatalError("No registered ViewController for model: \(name)")
        }

        guard let topController = Router.topViewController() else {
            return Future(error: BoarError.noTopController)
        }

        let vc = info.create()

        print("Show \(vc.className) on \(topController.className)")

        let parent = topController.container ?? topController.parentContainer() ?? self.container

        let container = parent.push()
        container.register(value: data)
        vc.container = container

        return showVCF(vc: vc, animated: flag)
    }

    @discardableResult
    public func showVCF<VC: UIViewController>(vc: VC,
                                              animated flag: Bool) -> Future<VC, NSError> {

        guard let topController = Router.topViewController() else {
            return Future(error: BoarError.noTopController)
        }

        if vc is JustPresent1 {
            return topController.reactive.present(vc, animated: true)
        }

        if let nav =  topController.navigationController {
            if !(vc is IgnorePrevous1) &&
                nav.viewControllers.count > 1 &&
                type(of: nav.viewControllers[nav.viewControllers.count - 2]) == type(of: vc) {

                return nav.reactive.pop(animated: flag).map { _ in vc}
            }
            return nav.reactive.push(vc: vc, animated: flag)
        } else {
            let nav = UINavigationController(rootViewController: vc)
            if let navConfig = nav as? NavigationConfigProtocol {
                navConfig.configure()
            }
            if var motion = vc as? MotionPresent1 {
                motion.isMotionEnabled = true
            }
            return topController.reactive.present(nav, animated: flag).map { _ in vc}
        }
    }

    //
    @discardableResult
    public func closeF(animated flag: Bool)-> Future<Void, NSError> {
        if let topController = Router.topViewController() {
            if let navigationController = topController.navigationController {
                if navigationController.viewControllers.first == topController {
                    return navigationController.reactive.dismiss(animated: flag)
                }
                return navigationController.reactive.pop(animated: flag)
            }
            return topController.reactive.dismiss(animated: flag)
        }
        return Future(error: BoarError.noTopController)
    }
    //
    @discardableResult
    public func closeAllF(animated flag: Bool)-> Future<Void, NSError> {
        if let topController = Router.topViewController() {
            if let navigationController = topController.findNavigationController() {
                return navigationController.reactive.dismiss(animated: flag)
            }
            if topController.presentingViewController != nil {
                return topController.reactive.dismiss(animated: flag)
            }
            return Future(value: ())
        }
        return Future(error: BoarError.noTopController)
    }
}
