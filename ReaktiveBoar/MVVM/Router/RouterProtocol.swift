//
//  RouterProtocol.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 9/1/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Bond
import ReactiveKit
import BrightFutures

public protocol NavigationConfigProtocol: class {
    func configure()
}

public protocol JustPresent1 { }
public protocol MotionPresent1 {
    var isMotionEnabled: Bool { get set }
}
public protocol IgnorePrevous1 { }
extension UIAlertController: JustPresent1 { }

public protocol PickerController {
    associatedtype Option
    var result: SafeSignal<Option> { get }
}

public protocol RouterProtocol1 {
    func show<VM: VCVMProtocol, Data>(_ vmType: VM.Type) -> Bond<Data>

    func showVC<VC: UIViewController>() -> Bond<VC>

    func showPicker<VC: UIViewController>(vc: VC) -> Signal<VC.Option, NSError> where VC: PickerController

    func close() -> Bond<Void>
    func closeAll() -> Bond<Void>

    func register<VC>(_ vcType: VC.Type) where VC: VCView, VC: UIViewController
    func showError<E: Error>(title: String,
                             msg: @escaping (E) -> String) -> Bond<E?>

    func showActions<E: ActionType>(_ type: E.Type, title: String, message: String) -> SafeSignal<E>

    @discardableResult
    func makeAsRootF<VM: VCVMProtocol, Data>(_ vmType: VM.Type,
                                             inNavigation: Bool,
                                             data: Data) -> Future<UIViewController, NSError>

    func makeAsRootF<VC: UIViewController, Data>(_ vc: VC,
                                                 inNavigation: Bool,
                                                 data: Data) -> Future<UIViewController, NSError>
}

public extension RouterProtocol1 {

    @discardableResult
    func makeAsRootF<VM: VCVMProtocol>(_ vmType: VM.Type, inNavigation: Bool) -> Future<UIViewController, NSError> {
        return makeAsRootF(vmType, inNavigation: inNavigation, data: ())
    }

    func show<VM: VCVMProtocol>(_ vmType: VM.Type) -> Bond<Void> {
        return show(vmType)
    }

    @discardableResult
    func makeAsRootF<VC: UIViewController, Data>(_ vcType: VC.Type,
                                                 inNavigation: Bool,
                                                 data: Data) -> Future<UIViewController, NSError> {

        let vc = UIStoryboard(name: vcType.className, bundle: Bundle(for: vcType))
            // swiftlint:disable:next force_cast
            .instantiateInitialViewController() as! VC
        return makeAsRootF(vc, inNavigation: inNavigation, data: data)
    }
    @discardableResult
    func makeAsRootF<VC: UIViewController>(_ vcType: VC.Type,
                                           inNavigation: Bool) -> Future<UIViewController, NSError> {

        return makeAsRootF(vcType, inNavigation: inNavigation, data: ())
    }
}
