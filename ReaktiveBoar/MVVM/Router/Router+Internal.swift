//
//  Router+Internal.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 6/4/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Bond
import ReactiveKit
import BrightFutures
import Swinject

extension Router {
    public static func topViewController(_ base: UIViewController?
        = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }

    fileprivate static func getVisibleViewController(from vc: UIViewController?
        = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = vc as? UINavigationController {
            return getVisibleViewController(from: navigationController.visibleViewController)
        } else if let tabController = vc as? UITabBarController {
            return getVisibleViewController(from: tabController.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return getVisibleViewController(from: pvc)
            } else {
                return vc
            }
        }
    }
    //
    public static func tabBarViewController(_ base: UIViewController?
        = UIApplication.shared.keyWindow?.rootViewController) -> UITabBarController? {

        if let tab = base as? UITabBarController {
            return tab
        } else {
            return base?.tabBarController
        }
    }
    //
    //    internal static func topView() -> UIView? {
    //        var view = topViewController()?.view
    //
    //        while view != nil && view?.superview != nil {
    //            view = view!.superview
    //        }
    //        return view
    //
    //    }
}
//
extension UIViewController {
    func findNavigationController() -> UINavigationController? {
        if let navigationController = navigationController {
            return navigationController
        }

        for controller in children {
            if let navigationController = controller.navigationController {
                return navigationController
            }
        }
        for controller in children {
            if let navigationController = controller.findNavigationController() {
                return navigationController
            }

        }
        return nil
    }
}

extension ReactiveExtensions where Base: UIViewController {

    func present<T: UIViewController>(_ vc: T, animated: Bool)->Future<T, NSError> {
        let promise = Promise<T, NSError>()
        base.present(vc, animated: animated) {
            promise.success(vc)
        }
        return promise.future
    }

    func dismiss(animated: Bool)->Future<Void, NSError> {
        let promise = Promise<Void, NSError>()
        base.dismiss(animated: animated) {
            promise.success(())
        }
        return promise.future
    }
}
//
extension ReactiveExtensions where Base: UINavigationController {

    func push<T: UIViewController>(vc: T, animated: Bool)->Future<T, NSError> {

        let promise = Promise<T, NSError>()

        CATransaction.begin(); defer { CATransaction.commit(); }
        CATransaction.setCompletionBlock {
            promise.success(vc)
        }
        base.pushViewController(vc, animated: animated)
        return promise.future
    }

    func pop(animated: Bool)->Future<Void, NSError> {

        let promise = Promise<Void, NSError>()

        CATransaction.begin(); defer { CATransaction.commit(); }
        CATransaction.setCompletionBlock {
            promise.success(())
        }
        base.popViewController(animated: animated)
        return promise.future
    }

    func popToRoot(animated: Bool)->Future<Void, NSError> {
        let promise = Promise<Void, NSError>()

        CATransaction.begin(); defer { CATransaction.commit(); }
        CATransaction.setCompletionBlock {
            promise.success(())
        }
        base.popToRootViewController(animated: animated)
        return promise.future
    }
}
