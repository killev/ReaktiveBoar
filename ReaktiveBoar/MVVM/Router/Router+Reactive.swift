//
//  Router+Reactive.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 6/4/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Bond
import ReactiveKit
import BrightFutures
import Swinject

extension Router {

    public func show<VM: VCVMProtocol, T>(_ vmType: VM.Type)-> Bond<T> {
        return Bond(target: self) { router, data in
            router.showF(VM.self, data: data, animated: true)
        }
    }
    public func close()-> Bond<Void> {
        return Bond(target: self) { router, _ in
            router.closeF(animated: true)
        }
    }
    public func closeAll()-> Bond<Void> {
        return Bond(target: self) { router, _ in
            router.closeAllF(animated: true)
        }
    }
    public func showVC<VC>() -> Bond<VC> where VC: UIViewController {
        return Bond(target: self) { router, vc in
            router.showVCF(vc: vc, animated: true)
        }
    }
}

public protocol ActionType: CaseIterable {
    var title: String { get }
    var style: UIAlertAction.Style { get }
}

extension Router {
    public func showError<E: Error>(title: String,
                                    msg: @escaping (E) -> String) -> Bond<E?> {

        return Bond(target: self) { router, error in
            if let error = error {
                let alert = UIAlertController(title: title, message: msg(error), preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                router.showVCF(vc: alert, animated: true)
            }
        }
    }

    public func showActions<E: ActionType>(_ type: E.Type, title: String, message: String) -> SafeSignal<E> {

        return SafeSignal { observer in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let actions = E.allCases.map { actionItem in
                UIAlertAction(title: actionItem.title,
                              style: actionItem.style,
                              handler: {_ in observer.receive(actionItem)})
            }
            actions.forEach(alert.addAction)
            self.showVCF(vc: alert, animated: true)

            return NonDisposable.instance
        }
    }
}
