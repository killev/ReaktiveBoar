//
//  UIView.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 5/17/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReactiveKit

public protocol ReuseView: View, ReuseDisposeBagProvider {
    var vm: VMType? { get set }
}

private var vmKey = ""
extension ReuseView where Self: UIView {
    public var vm: VMType? {
        get {
            return Associated<Self,
                VMType?>(self, &vmKey)
                .value()
        }
        set {
            Associated<Self,
                VMType?>(self, &vmKey)
                .value(new: newValue)
            reuseBag.dispose()
            unadvise(bag: reuseBag)

            if let vm = vm {
                advise(vm: vm, bag: reuseBag)
            }
        }
    }
}

private var reuseBagKey = ""
extension ReuseDisposeBagProvider {

    public var reuseBag: DisposeBag {
        return Associated<Self,
            DisposeBag>(self, &reuseBagKey)
            .value(initial: DisposeBag.init)
    }
}
