//
//  UICollectionViewCell+Reactive.swift
//  ReaktiveBoar
//
//  Created by Peter Ovchinnikov on 3/27/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import Bond
import ReactiveKit

extension UIView {

    func fillAllSuperview() {
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint(item: self,
                               attribute: NSLayoutConstraint.Attribute.top,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.superview,
                               attribute: NSLayoutConstraint.Attribute.top,
                               multiplier: 1,
                               constant: 0),

            NSLayoutConstraint(item: self,
                               attribute: NSLayoutConstraint.Attribute.bottom,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.superview,
                               attribute: NSLayoutConstraint.Attribute.bottom,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: self,
                               attribute: NSLayoutConstraint.Attribute.leading,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.superview,
                               attribute: NSLayoutConstraint.Attribute.leading,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: self,
                               attribute: NSLayoutConstraint.Attribute.trailing,
                               relatedBy: NSLayoutConstraint.Relation.equal,
                               toItem: self.superview,
                               attribute: NSLayoutConstraint.Attribute.trailing,
                               multiplier: 1,
                               constant: 0)
        ]

        self.superview!.addConstraints(constraints)
        self.superview!.setNeedsLayout()
        self.superview!.updateConstraints()

    }
}
extension UICollectionViewCell {
    private static let tag = 1002312
    var selectButton: UIButton {
        if let button = (self.contentView.subviews
            .first { $0.tag == UICollectionViewCell.tag } as? UIButton) {
            return button
        }
        let button = UIButton()
        button.tag = 1002312
        self.contentView.addSubview(button)
        button.fillAllSuperview()
        button.isAccessibilityElement = true
        button.accessibilityIdentifier = self.className + "SelectionButton"
        self.sendSubviewToBack(button)
        return button
    }
}
public extension UICollectionView {
    func scrollToBottom(animated: Bool) {
        if numberOfItems(inSection: numberOfSections - 1) > 0 {
            let index = IndexPath(row: numberOfItems(inSection: numberOfSections - 1) - 1,
                                  section: numberOfSections - 1)
            scrollToItem(at: index, at: .bottom, animated: animated)
        }
    }
}

public extension ReactiveExtensions where Base: UICollectionViewCell {

    var didSelect: SafeSignal<Void> {
        return self.base.selectButton.reactive.tap
    }
}

public extension ReactiveExtensions where Base: UICollectionView {
    func visibleCell(at: UICollectionView.ScrollPosition) -> Bond<IndexPath> {
        return self.bond { view, index in
            view.scrollToItem(at: index, at: at, animated: true)
        }
    }
}

public extension ReactiveExtensions where Base: UICollectionView {
    var scrollToBottom: Bond<Void> {

        return self.bond { view, _ in

            if view.window != nil && view.numberOfSections > 0 {
                if view.numberOfItems(inSection: view.numberOfSections - 1) > 0 {
                    let index = IndexPath(row: view.numberOfItems(inSection: view.numberOfSections - 1) - 1,
                                          section: view.numberOfSections - 1)
                    view.scrollToItem(at: index, at: .bottom, animated: false)
                }
            }
        }
    }
}

extension UICollectionView {
    public func getCurrentIndexPath() -> IndexPath? {
        let offset = contentOffset
        if let cell = visibleCells.filter({ $0.frame.contains(offset) }).first {
            return indexPath(for: cell)
        }
        return nil
    }
}

public extension ReactiveExtensions where Base: UICollectionViewCell {
    var isSelected: SafeSignal<Bool> {
        return keyPath(\.isSelected)
    }
}
