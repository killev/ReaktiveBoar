//
//  NotificationCenter+Reactive.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 6/4/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond

public extension ReactiveExtensions where Base: NotificationCenter {
    func post<T>(name: NSNotification.Name) -> Bond<T> {
        return bond(context: .immediate) { center, object in
            center.post(name: name, object: object)
        }
    }

    func post(name: NSNotification.Name) -> Bond<Void> {
        return bond(context: .immediate) { center, _ in
            center.post(name: name, object: nil)
        }
    }

    var keyboardHeight: SafeSignal<CGFloat> {

        let willShow = self.notification(name: UIResponder.keyboardWillChangeFrameNotification)
            .map { notification -> CGFloat in
                if let userInfo = notification.userInfo,
                    let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    return keyboardFrame.height
                }
                return 0
            }

        let willHide = notification(name: UIResponder.keyboardWillHideNotification)
            .map { _ -> CGFloat in
                return 0
        }
        return merge(willShow, willHide).prepend(0)
    }
}
