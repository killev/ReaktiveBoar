//
//  UIImagePicker.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 9/4/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReactiveKit

public enum ImagePickerResult {
    case image(UIImage)
    case cancel
}

class ImagePickerControllerDelegate: VM, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var observer: AtomicObserver<ImagePickerResult, Never>?

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let selectedImage = info[.originalImage] as? UIImage {
            observer?.receive(.image(selectedImage))
        } else {
            observer?.receive(completion: .finished)
            //Router.close(animated: true)
        }

    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        observer?.receive(lastElement: .cancel)
    }

    deinit {
        print("ImagePickerControllerDelegate deinit")
    }
}

extension UIImagePickerController: PickerController, JustPresent1 {

    static var delegateKey = "_delegate"

    public var result: Signal<ImagePickerResult, Never> {
        let delegate = ImagePickerControllerDelegate()
        objc_setAssociatedObject(self,
                                 &UIImagePickerController.delegateKey,
                                 delegate, .OBJC_ASSOCIATION_RETAIN)
        self.delegate = delegate
        return SafeSignal { observer in
            delegate.observer = observer
            return NonDisposable.instance
        }
    }

    public typealias Option = ImagePickerResult

}
