//
//  Future+ReactiveKit.swift
//  ReaktiveBoarTests
//
//  Created by Peter Ovchinnikov on 3/20/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import BrightFutures
import ReactiveKit
import Bond

public extension Future {

    var val: Property<T?> {
        let prop = Property<T?>(nil)
        self.onSuccess { prop.send($0) }
        return prop
    }

    func sig(init: (() -> T)? = nil) -> Signal<T, E> {
        return Signal { observer in
            if !self.isCompleted {
                if let  `init` = `init` {
                    observer.receive(`init`())
                }
            }

            let token = InvalidationToken()
            self.onSuccess(token.validContext) { observer.receive(lastElement: $0) }
            self.onFailure(token.validContext) { observer.receive(completion: .failure($0)) }
            return BlockDisposable {
                token.invalidate()
            }
        }
    }
}

public extension Future {
    func safeSig<Err: BindableProtocol>(error: Err) -> SafeSignal<T> where Err.Element == E? {
        return SafeSignal { observer in
            let errorSignal = SafeSignal<E?> { errorObserver in
                let token = InvalidationToken()
                self.onComplete(token.validContext, callback: { result in
                    _ = result.analysis(
                        ifSuccess: { val -> Void in
                            errorObserver.receive(lastElement: nil)
                            observer.receive(lastElement: val)
                    },
                        ifFailure: { err -> Void in
                            errorObserver.receive(lastElement: err)
                            observer.receive(completion: .finished)
                    })
                })
                return BlockDisposable {
                    token.invalidate()
                }
            }
            _ = error.bind(signal: errorSignal)
            return  NonDisposable.instance
        }
    }
    func resultSig() -> SafeSignal<Result<T, E>> {
        return SafeSignal { observer in
            let token = InvalidationToken()
            self.onComplete(token.validContext, callback: { result in
                observer.receive(result)
            })
            return BlockDisposable {
                token.invalidate()
            }
        }
    }
    func working<W: BindableProtocol>(working: W) -> Future<T, E> where W.Element == Bool {

        let workingSignal = SafeSignal<Bool> { woringObserver in
            if self.isCompleted {
                woringObserver.receive(lastElement: false)
                return woringObserver
            }
            woringObserver.receive(true)
            self.onComplete(callback: { _ in
                woringObserver.receive(lastElement: false)
            })
            return woringObserver
        }
        _ = working.bind(signal: workingSignal)
        return self
    }
}
public extension SignalProtocol where Element: ResultProtocol, Error == Never {
    func splitBind<L: BindableProtocol, O: BindableProtocol>(error: O, value: L)
            -> Disposable where L.Element == Element.Value, O.Element == Element.Error? {
        let disposable = CompositeDisposable()
        disposable += self.map { result-> Element.Value? in
            result.analysis(ifSuccess: { val->Element.Value? in val },
                            ifFailure: { _ -> Element.Value? in nil })
            }.ignoreNils().bind(to: value)
        disposable += self.map { result-> Element.Error? in
            result.analysis(ifSuccess: { _->Element.Error? in nil },
                            ifFailure: { err -> Element.Error? in err })
            }.bind(to: error)
        return disposable
    }
    func safe<L: BindableProtocol>(error: L)
        ->SafeSignal<Element.Value> where L.Element == Element.Error? {

            return SafeSignal { observer in
                let errorStream = SafeSignal<Element.Error?> { errorObserver in
                    return self.observe { val in
                        switch val {
                        case .next(let value): value.analysis(ifSuccess: {
                            observer.receive($0)
                            errorObserver.receive(nil)
                        }, ifFailure: { errorObserver.receive($0) })
                        case .completed:
                            observer.receive(completion: .finished)
                        case .failed: fatalError() // it should never happen
                        }
                    }
                }
                return CompositeDisposable([observer,
                                            error.bind(signal: errorStream)])
            }
    }
}

public extension SignalProtocol where Error == Never {
    /// Establish a one-way binding between the source and the bindable.
    /// - Parameter bindable: A binding target that will receive signal events.
    /// - Returns: A disposable that can cancel the binding.
    @discardableResult
    func bind<B: BindableProtocol>(toMany bindables: [B]) -> Disposable where B.Element == Element {
        let compositeDisposable = CompositeDisposable()
        bindables.forEach {
            compositeDisposable += $0.bind(signal: toSignal())
        }
        return compositeDisposable
    }
    /// Establish a one-way binding between the source and the bindable.
    /// - Parameter bindable: A binding target that will receive signal events.
    /// - Returns: A disposable that can cancel the binding.
    @discardableResult
    func bind<B: BindableProtocol>(toMany bindables: [B])
            -> Disposable where B.Element: OptionalProtocol, B.Element.Wrapped == Element {
        let compositeDisposable = CompositeDisposable()
        bindables.forEach {
            compositeDisposable += map { B.Element($0) }.bind(to: $0)
        }
        return compositeDisposable
    }
}
