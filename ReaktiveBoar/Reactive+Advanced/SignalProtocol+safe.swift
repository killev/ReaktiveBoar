//
//  SignalProtocol+safe.swift
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 9/3/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReactiveKit

public extension SignalProtocol {
    func safe<L: BindableProtocol>(error: L)
        ->SafeSignal<Element> where L.Element == Error? {

            return SafeSignal { observer in
                let errorStream = SafeSignal<Error?> { errorObserver in
                    return self.observe { val in
                        switch val {
                        case .next(let value):
                            observer.receive(value)
                            errorObserver.receive(nil)
                        case .completed:
                            observer.receive(completion: .finished)
                        case .failed(let error):
                            errorObserver.receive(error)
                            observer.receive(completion: .finished)
                        }
                    }
                }
                return CompositeDisposable([observer,
                                            error.bind(signal: errorStream)])
            }
    }
}
