//
//  SignalProtocol+AdvancedOperators.swift
//  ReaktiveBoar
//
//  Created by Peter Ovchinnikov on 3/22/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

import ReactiveKit
public extension SignalProtocol where Element: OptionalProtocol {

    func recoverNils(_ def: @autoclosure @escaping ()->Element.Wrapped)-> Signal<Element.Wrapped, Error> {
        return self.map { $0._unbox ?? def() }
    }
}
public extension SignalProtocol {
    func flatMapLatest<O: SignalProtocol,
        W: BindableProtocol>(working: W, _ transform: @escaping (Element) -> O)
        -> Signal<O.Element, Error> where W.Element == Bool, O.Error == Error {

        var prev: Disposable = SafeSignal(just: false).bind(to: working)
        return flatMapLatest { el -> O in
            prev.dispose()
            let res = transform(el)
            let workingSignal = SafeSignal<Bool> { observer in
                observer.receive(true)
                let disposable = res.first().observe { _ in
                    observer.receive(false)
                    prev.dispose()
                }
                return disposable
            }
            prev = working.bind(signal: workingSignal)
            return res
        }
    }
}
