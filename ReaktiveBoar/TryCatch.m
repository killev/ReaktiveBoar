//
//  TryCatch.m
//  ReaktiveBoar-iOS
//
//  Created by Peter Ovchinnikov on 6/12/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "TryCatch.h"

@implementation TryCatch

/**
 Provides try catch functionality for swift by wrapping around Objective-C
 */
+ (void)try:(__attribute__((noescape))  void(^ _Nullable)(void))try catch:(__attribute__((noescape)) void(^ _Nullable)(NSException*exception))catch finally:(__attribute__((noescape)) void(^ _Nullable)(void))finally {
    @try {
        if (try != NULL) try();
    }
    @catch (NSException *exception) {
        if (catch != NULL) catch(exception);
    }
    @finally {
        if (finally != NULL) finally();
    }
}

+ (void)throwString:(NSString*)s
{
    @throw [NSException exceptionWithName:s reason:s userInfo:nil];
}

+ (void)throwException:(NSException*)e
{
    @throw e;
}

@end
