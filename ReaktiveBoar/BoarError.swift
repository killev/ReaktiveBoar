//
//  BoarError.swift
//  ReaktiveBoar
//
//  Created by Peter Ovchinnikov on 3/14/19.
//  Copyright © 2019 Peter Ovchinnikov. All rights reserved.
//

public struct BoarError {

}

public extension BoarError {
    static let Domain = "io.reaktiveboar"

    static func err(_ code: Int, userInfo: [String: Any]? = nil) -> NSError {
        return NSError(domain: self.Domain, code: code, userInfo: userInfo)
    }
    static let noTopControllerCode: Int    = 90002
    static let noTopController: NSError = err(noTopControllerCode,
                                                     userInfo: ["Message": "No top conroller found"])

    static let noNavigationControllerCode: Int    = 90003
    static let noNavigationController: NSError = err(noNavigationControllerCode,
                                                            userInfo: ["Message": "No navigation conroller found"])

    static let noKeyWindowCode: Int    = 90004
    static let noKeyWindow: NSError = err(noKeyWindowCode,
                                              userInfo: ["Message": "No key window registered in the app"])

    static let animationFailedCode: Int = 90005
    static func animationFailed(_ reason: String) -> NSError {
        return err(animationFailedCode,
                   userInfo: ["Message": "Animation failed: \(reason)"])
    }

}
