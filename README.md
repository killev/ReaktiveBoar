[![pipeline status](https://gitlab.com/killev/ReaktiveBoar/badges/master/pipeline.svg)](https://gitlab.com/killev/ReaktiveBoar/commits/master)
[![coverage report](https://gitlab.com/killev/ReaktiveBoar/badges/master/coverage.svg)](https://gitlab.com/killev/ReaktiveBoar/commits/master)

# MVVM BASED ARCHITECTURE FRAMEWORK

- Architecture
  - Dependency Injection
  - Stack based
  - Managed state (database as state)
  - Immutable data + Alamofire
  - MVVM (https://gitlab.com/killev/ReaktiveBoar) 
  - CollectionView based
  - Router as enpoint